package PT2019.Assignment1.Assignment1;

import javax.swing.JFrame;

public class Monom {
	
	public float coeff;
	public int degree;
	
	public Monom(float coeff, int f) {
		this.coeff=coeff;
		this.degree=f;
		
	}
	
	public void setCoeff(float coeff2) {
		coeff=coeff2;
	}

	public float getCoeff() {
		return coeff;
	}
	
	public void setDegree(int degree2) {
		degree=degree2;
	}
	
	public int getDegree() {
		return degree;
	}
	
	
}
