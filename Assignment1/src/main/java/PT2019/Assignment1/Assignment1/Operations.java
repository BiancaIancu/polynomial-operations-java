package PT2019.Assignment1.Assignment1;

import java.util.Collections;

import javax.swing.JTextField;

public class Operations {
	
	//NON MULTIPLE DEGREES
		public static Polynom degrees(Polynom p) {
			
			Polynom rp = new Polynom();
			
			int i=0;
			while(i < p.monoms.size()-1) {
				float coeff = p.monoms.get(i).getCoeff();
				int j = i+1;
				while(p.monoms.get(j).getDegree() == p.monoms.get(i).getDegree()) {
					coeff = coeff + p.monoms.get(j).getCoeff();
					j++;
				}
				Monom maux = new Monom(coeff,p.monoms.get(i).getDegree());
				rp.monoms.add(maux);
				i=j;
			}
			
			for(Monom m: rp.monoms) {
				if(m.getDegree()==0) {
					System.out.print(m.getCoeff()+"x^"+m.getDegree());
				}
				else {
				System.out.print(m.getCoeff()+"x^"+m.getDegree()+" + ");
				}
			}
			
			return rp;
		}
		
	//END
	
//SORT
	public static Polynom sort (Polynom rp) {
		for(int i = 0; i < rp.monoms.size() - 1; i++) {
			for(int j = i+1; j < rp.monoms.size(); j++) {
				
				if(rp.monoms.get(i).getDegree() < rp.monoms.get(j).getDegree()) {
					Collections.swap(rp.monoms, i, j);
					
				}
				
			}
		}
		//return degrees(rp);
		return rp;
		
	}
//END SORT
	

//SUM
	public static Polynom sum (Polynom p1, Polynom p2) {
		Polynom rp = new Polynom();
		Polynom rp2 = new Polynom();
		
		//put in rp all the sums of monoms which has the same coeff
		for(Monom i: p2.monoms) {
			Monom maux = new Monom(i.getCoeff(),i.getDegree());
		
			for(Monom j:p1.monoms) {
	
				if(maux.getDegree() == j.getDegree()) {
					maux.setCoeff(maux.getCoeff()+j.getCoeff());
				}
			}
			
			rp.monoms.add(maux);
		}
		
		
		//complete with the single monoms from p1
		for(Monom i: rp.monoms) {
			rp2.monoms.add(i);
		}
		
		
		for(Monom i: p1.monoms) {
			int ok=0;
			for(Monom j: rp.monoms) {
				if(i.getDegree() == j.getDegree()) {
					ok=1;
				}
				
			}
			
			if(ok == 0) {
				rp2.monoms.add(i);
			}
		}
		
		return sort(rp2);
	}
	
		
//ENDSUM
	
//SUBSTRACTION
		public static Polynom sub (Polynom p1, Polynom p2) {
			
			Polynom rp = new Polynom();
			Polynom rp2 = new Polynom();
			
			//put in rp all the substitutions of monoms which has the same coeff
			for(Monom i: p1.monoms) {
				Monom maux = new Monom(i.getCoeff(), i.getDegree());
				
				for(Monom j:p2.monoms) {
					
					if(maux.getDegree()==j.getDegree()) {
						maux.setCoeff(maux.getCoeff()-j.getCoeff());
					}
				}
				
				rp.monoms.add(maux);
			}
			
			//complete with the single monoms from p2
			for(Monom i:rp.monoms) {
				rp2.monoms.add(i);
			}
				
			
			for(Monom i: p2.monoms) {
				int ok=0;
				for(Monom j: rp.monoms) {
					if(i.getDegree()==j.getDegree()) {
						ok=1;
					}
				}
				
				if(ok==0) {
					Monom maux2 = new Monom(-i.getCoeff(), i.getDegree());
					rp2.monoms.add(maux2);
				}
			}
			
			//return sort(degrees(rp2));	
			return sort(rp2);
		}
//END SUBSTRACTION
		
		
//DERIVATION
		public static Polynom deriv(Polynom p1) {
			Polynom rp = new Polynom();
			
			for (Monom i: p1.monoms) {
				int degreeaux = i.getDegree();
				float coeffaux=i.getCoeff();
				
				if(degreeaux==0) {
					Monom maux = new Monom(0,0) ;
					rp.monoms.add(maux);
				}
				else {
					Monom maux = new Monom(degreeaux*coeffaux, degreeaux-1);
					rp.monoms.add(maux);
				}
				
			}
			
			return rp;
		}
//ENDDERIVATION
		
		
//INTEGRATION
		
		public static Polynom integ(Polynom p1) {
			Polynom rp = new Polynom();
			
			for(Monom i: p1.monoms) {
				int degreeaux = i.getDegree();
				float coeffaux=i.getCoeff();
				
				Monom maux = new Monom(coeffaux/(degreeaux+1), degreeaux+1);
				rp.monoms.add(maux);
			}
			
			
			return sort(rp);
		}
//ENDINTEGRATIONS

//PRODUCT
		public static Polynom product(Polynom p1, Polynom p2) {
			Polynom rp = new Polynom();
			//Polynom rp2 = new Polynom();
			
			for(Monom i: p1.monoms) {
				float coeff1 = i.getCoeff();
				int degree1 = i.getDegree();
				
				for(Monom j: p2.monoms) {
					float coeff2 = j.getCoeff();
					int degree2 = j.getDegree();
					
					Monom maux = new Monom(coeff1*coeff2, degree1+degree2);
					rp.monoms.add(maux);
						
				}
			}

			return sort(rp);
		}
//ENDPRODUCT
		
//PARSE
		public static void ParsePolynom(Polynom p, JTextField text) {
			String polynomText1 = text.getText();
			//JOptionPane.showMessageDialog(null, polynomText1);
			
			
			String[] polynomSplit1 = polynomText1.split("\\+");
			//polynomSplit contine monoamele
			for(String m1: polynomSplit1) {
				
				//fiecare monom e impartit in 2
				String[] monomSplit1 = m1.split("x");
				
				float coefficient1 = Float.parseFloat(monomSplit1[0]);
				
				String degreeString1 = monomSplit1[1].replace("^","");
				
				int degree1 = Integer.parseInt(degreeString1);
			
				
				Monom monom1 = new Monom(coefficient1,degree1);
				p.monoms.add(monom1);
			}
			
			
			
		}		
//endparse
		
}
