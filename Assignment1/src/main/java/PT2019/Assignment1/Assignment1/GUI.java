package PT2019.Assignment1.Assignment1;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.print.attribute.standard.JobMessageFromOperator;
import javax.swing.*;

public class GUI {
	
	public void init() {
		
				//layouts
				FlowLayout flowLayout = new FlowLayout();
				

				
		//frame
				JFrame frame = new JFrame ("Polynomnial Operations");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(500,500);
				
			//panel1 - First Pol
				JPanel panel1 = new JPanel();
				//panel1.setLayout(flowLayout);

				JLabel label1 = new JLabel("Introduceti primul polinom:");
				
				final JTextField text1 = new JTextField();
		        Font bigFont = text1.getFont().deriveFont(Font.PLAIN, 20f);
				text1.setColumns(10);
				text1.setFont(bigFont);

				panel1.add(label1);
				panel1.add(text1);
				
			//panel2 - Second Pol
				JPanel panel2 = new JPanel();
				//panel2.setLayout(flowLayout);
				
				JLabel label2 = new JLabel("Introduceti al doilea polinom:");
				
				final JTextField text2 = new JTextField("");
				Font bigFont2 = text2.getFont().deriveFont(Font.PLAIN, 20f);
				text2.setColumns(10);
				text2.setFont(bigFont);

					
				panel2.add(label2);
				panel2.add(text2);
				
			//panel 3 - Result
				JPanel panel3 = new JPanel();
				panel3.setLayout(flowLayout);
				
				JLabel label3 = new JLabel("Rezultat:");
				
				final JTextField text3 = new JTextField("");
				Font bigFont3 = text3.getFont().deriveFont(Font.PLAIN, 20f);
				text3.setColumns(10);
				text3.setFont(bigFont);
				
				panel3.add(label3);
				panel3.add(text3);

				//panel 4 - buttons
				JPanel panel4 = new JPanel();
				
			    JButton b1 = new JButton("Adunare");
				JButton b2 = new JButton("Scadere");
				JButton b3 = new JButton("Derivare");
				JButton b4 = new JButton("Integrare");
				JButton b5 = new JButton("Inmultire");
				
			
				panel4.add(b1);
				panel4.add(b2);
				panel4.add(b3);
				panel4.add(b4);
				panel4.add(b5);
			
				
			final Polynom p1 = new Polynom();
			final Polynom p2 = new Polynom();

			

			//panel 4 - listeners
				
				
				b1.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						
					
						Operations.ParsePolynom(p1, text1);
						Operations.ParsePolynom(p2, text2);
						Polynom sumPolynom = new Polynom();
						
						sumPolynom = Operations.sum(p1, p2);
						
						String s ="";
						for(Monom m: sumPolynom.monoms) {
							
							s = s + m.getCoeff()+"x^"+m.getDegree()+"+";
							
						}
						
						int j = s.lastIndexOf('+');
						s = s.substring(0,j)+"";
						text3.setText(s);
						JOptionPane.showMessageDialog(null, s);
						s="";
					
						 
					}
				});
				
				
				b2.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						
					
						Operations.ParsePolynom(p1, text1);
						Operations.ParsePolynom(p2, text2);
						Polynom subPolynom = new Polynom();
						
						subPolynom = Operations.sub(p1, p2);
						
						String s ="";
						for(Monom m: subPolynom.monoms) {
							
							s = s + m.getCoeff()+"x^"+m.getDegree()+"+";
							
						}
						
						int j = s.lastIndexOf('+');
						s = s.substring(0,j)+"";
						
						text3.setText(s);
						JOptionPane.showMessageDialog(null, s);
						 s="";
 
					}
				});
				
				b3.addActionListener(new ActionListener() {
					
					
					public void actionPerformed(ActionEvent e) {
						
					
						Operations.ParsePolynom(p1, text1);
						Polynom derivPolynom = new Polynom();
						
						derivPolynom = Operations.deriv(p1)	;
						
						String s ="";
						for(Monom m: derivPolynom.monoms) {
							
							s = s + m.getCoeff()+"x^"+m.getDegree()+"+";
							
						}
						int j = s.lastIndexOf('+');
						s = s.substring(0,j)+"";
						text3.setText(s);

						JOptionPane.showMessageDialog(null, s);
						s="";

					}
				});
				
				b4.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						
					
						Operations.ParsePolynom(p1, text1);
						Polynom integratePolynom = new Polynom();
						
						integratePolynom = Operations.integ(p1);
						
						String s ="";
						for(Monom m: integratePolynom.monoms) {
							
							s = s + m.getCoeff()+"x^"+m.getDegree()+"+";
							
						}
						int j = s.lastIndexOf('+');
						s = s.substring(0,j)+"";
						text3.setText(s);
						JOptionPane.showMessageDialog(null, s);
						s="";

					}
				});
				
				b5.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						
					
						Operations.ParsePolynom(p1, text1);
						Operations.ParsePolynom(p2, text2);
						Polynom productPolynom = new Polynom();
						
						productPolynom = Operations.product(p1, p2);
						
						String s ="";
						for(Monom m: productPolynom.monoms) {
							
							s = s + m.getCoeff()+"x^"+m.getDegree()+"+";
							
						}
						int j = s.lastIndexOf('+');
						s = s.substring(0,j)+"";
						text3.setText(s);

						JOptionPane.showMessageDialog(null, s);
						s="";

					}
				});
				
				
				
				//main panel
				JPanel panel = new JPanel();
				BoxLayout polynombox = new BoxLayout(panel,BoxLayout.Y_AXIS);
				panel.setLayout(polynombox);
				panel.add(panel1);
				panel.add(panel2);
				panel.add(panel4);
				panel.add(panel3);
				
				frame.setContentPane(panel);
				frame.setVisible(true);
				
			}
	
	
	

	}
	
	

