package PT2019.Assignment1.Assignment1;

import static org.junit.Assert.*;

import org.junit.Test;

public class operationTest {

	Polynom p1 = new Polynom();
	Polynom p2 = new Polynom();
	
	Polynom p3 = new Polynom();
	Polynom p4 = new Polynom();
	
	Polynom rez = new Polynom();
	Polynom rez2 = new Polynom();
	Polynom rez3 = new Polynom();
	Polynom rez4 = new Polynom();
	Polynom rez5 = new Polynom();

	Monom m1 = new Monom(2,5);
	Monom m2 = new Monom(5,1);
	Monom m3 = new Monom(3,0);
	Monom m4 = new Monom(2,3);
	Monom m5 = new Monom(1,2);
	Monom m6 = new Monom(2,1);
	Monom m7 = new Monom(1,0);
	
	Monom m8 = new Monom(2,1);
	Monom m9 = new Monom(2,2);
	Monom m10 = new Monom(1,2);
	
	

	@Test
	public void test() {
			p1.monoms.add(m1);
			p1.monoms.add(m2);
			p1.monoms.add(m3);
			
			p2.monoms.add(m4);
			p2.monoms.add(m5);
			p2.monoms.add(m6);
			p2.monoms.add(m7);
			
			p3.monoms.add(m8);
			p3.monoms.add(m9);
			p4.monoms.add(m10);

			
			rez=Operations.sum(p1,p2);
			rez2=Operations.sub(p1,p2);
			rez3=Operations.deriv(p1);
			rez4=Operations.integ(p1);
			rez5=Operations.product(p1, p2);
			
			for(Monom m: rez.monoms) {
				if(m.getDegree()==0) {
					System.out.print(m.getCoeff()+"x^"+m.getDegree());
				}
				else {
				System.out.print(m.getCoeff()+"x^"+m.getDegree()+" + ");
				}
			}
			
			System.out.println();		
			
			for(Monom m: rez2.monoms) {
				if(m.getDegree()==0) {
					System.out.print(m.getCoeff()+"x^"+m.getDegree());
				}
				else {
				System.out.print(m.getCoeff()+"x^"+m.getDegree()+" + ");
				}
			}
			
			System.out.println();	
			
			for (Monom m:rez3.monoms) {
				if(m.getDegree()==0) {
					System.out.print(m.getCoeff()+"x^"+m.getDegree());
				}
				else {
				System.out.print(m.getCoeff()+"x^"+m.getDegree()+" + ");
				}
			}
			
			System.out.println();	

			for (Monom m:rez4.monoms) {
				if(m.getDegree()==0) {
					System.out.print(m.getCoeff()+"x^"+m.getDegree());
				}
				else {
				System.out.print(m.getCoeff()+"x^"+m.getDegree()+" + ");
				}
			}
			
			System.out.println();	

			for (Monom m:rez5.monoms) {
				if(m.getDegree()==0) {
					System.out.print(m.getCoeff()+"x^"+m.getDegree());
				}
				else {
				System.out.print(m.getCoeff()+"x^"+m.getDegree()+" + ");
				}
			}
	}

}
